package br.com.sspds;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication(scanBasePackages={"br.com.sspds"})
public class SpringBootConfiguration {
	public static void main(String[] args){
	    SpringApplication.run(SpringBootConfiguration.class, args);
	}
	
}
