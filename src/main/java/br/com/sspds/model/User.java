package br.com.sspds.model;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

public class User {
	
	private Long id;
	private String name;
	@JsonFormat(pattern="dd/MM/yyyy HH:mm:ss")
    private Date dateCreate;
    private List<UserPermission> list;
    
    
    public User() {
        super();
    }
    
	public User(Long id, String name, Date dateCreate, List<UserPermission> list) {
		this.id = id;
		this.name = name;
		this.dateCreate = dateCreate;
		this.list = list;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getDateCreate() {
		return dateCreate;
	}
	public void setDateCreate(Date dateCreate) {
		this.dateCreate = dateCreate;
	}
	public List<UserPermission> getList() {
		return list;
	}
	public void setList(List<UserPermission> list) {
		this.list = list;
	}
    
   
	
    
}
