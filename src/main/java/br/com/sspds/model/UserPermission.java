package br.com.sspds.model;


public class UserPermission {
	
	private String path;
	private String role;
	
	public UserPermission() {
		super();
	}
	
	public UserPermission(String path, String role) {
		this.path = path;
		this.role = role;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
	

}
