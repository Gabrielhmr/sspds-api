package br.com.sspds.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.sspds.model.User;
import br.com.sspds.model.UserPermission;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;

@Api(tags = { "afis" }, description = "Operacoes de pesquisa ")
@SwaggerDefinition(tags = { @Tag(name = "Afis") })
@RequestMapping("afis")
@RestController
public class UserController {
	
	private Map<Long, User> users;
	
	public UserController() {
		
		users = new HashMap<Long, User>();
		
		UserPermission userPermission1 = new UserPermission("login", "user");
		UserPermission userPermission2 = new UserPermission("home", "user");
		
		List<UserPermission> userPermissionsList = new ArrayList<UserPermission>();
		
		userPermissionsList.add(userPermission1);
		userPermissionsList.add(userPermission2);
		
		User u1 = new User(1L,"Gabriel", new Date(), userPermissionsList);
		
		users.put(1l, u1);

	}
	@ApiOperation(value = "Busca por digital no afis", notes = "Opcao utilizada para listar pessoas através de busca no afis ")
	 @RequestMapping(value = "users", method = RequestMethod.GET)
	  public ResponseEntity<List<User>> listar() {
	    return new ResponseEntity<List<User>>(new ArrayList<User>(users.values()), HttpStatus.OK);
	  }
	
	 @RequestMapping(value = "user/{id}", method = RequestMethod.GET)
	  public ResponseEntity<User> listar(@PathVariable (value = "id") Long Id) {
	    return new ResponseEntity<User>(users.get(Id), HttpStatus.OK);
	  }
	 
	 //@RequestMapping(value = "user", method = RequestMethod.POST, produces = "application/json") 
	 @PostMapping("user")
	 public User createUser(@RequestBody User user) {
	        return users.put(2l,user);
	 }
	 
	 @RequestMapping(value = "user/{id}", method = RequestMethod.DELETE) 
	 public User deleteUser(@PathVariable (value = "id") Long Id) {
	        return users.remove(Id);
	 }

}
